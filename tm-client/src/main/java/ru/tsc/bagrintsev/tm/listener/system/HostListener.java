package ru.tsc.bagrintsev.tm.listener.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.system.HostRequest;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerHostResponse;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public class HostListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String description() {
        return "Print server host.";
    }

    @Override
    @SneakyThrows
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        @Nullable final ServerHostResponse response = systemEndpoint.getHost(new HostRequest());
        @NotNull final String host = String.format("Current-call server host: %s", response.getHost());
        System.out.println(host);
    }

    @EventListener(condition = "@hostListener.name() == #consoleEvent.name")
    public void listenName(@NotNull ConsoleEvent consoleEvent) {
        handle(consoleEvent);
    }

    @EventListener(condition = "@hostListener.shortName() == #consoleEvent.name")
    public void listenShort(@NotNull ConsoleEvent consoleEvent) {
        handle(consoleEvent);
    }

    @NotNull
    @Override
    public String name() {
        return "host";
    }

    @NotNull
    @Override
    public String shortName() {
        return "-gh";
    }

}
