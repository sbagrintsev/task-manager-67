package ru.tsc.bagrintsev.tm.listener.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSetRoleRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class UserSetRoleListener extends AbstractUserListener {

    @NotNull
    @Override
    public String description() {
        return "Set role to user.";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@userSetRoleListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        showParameterInfo(EntityField.LOGIN);
        @NotNull final String login = TerminalUtil.nextLine();
        showParameterInfo(EntityField.ROLE);
        System.out.println(Arrays.toString(Role.values()));
        @NotNull final Role role = Role.toRole(TerminalUtil.nextLine());
        @NotNull final UserSetRoleRequest request = new UserSetRoleRequest(getToken());
        request.setRole(role);
        request.setLogin(login);
        userEndpoint.setRole(request);
    }

    @NotNull
    @Override
    public String name() {
        return "user-set-role";
    }

}
