package ru.tsc.sbagrintsev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.stereotype.Controller;
import ru.tsc.bagrintsev.tm.api.client.TaskCollectionRestEndpointClient;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.sbagrintsev.tm.marker.IntegrationCategory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@Category(IntegrationCategory.class)
public class TaskCollectionEndpointTest {

    @NotNull
    final TaskCollectionRestEndpointClient taskClient = TaskCollectionRestEndpointClient.client();

    @NotNull
    final Task task1 = new Task();

    @NotNull
    final Task task2 = new Task();

    @NotNull
    final Task task3 = new Task();

    @NotNull
    final Task task4 = new Task();

    @NotNull
    final Task task5 = new Task();

    List<String> resList = new ArrayList<>();

    @Before
    public void init() {
        task1.setName("Task1");
        task2.setName("Task2");
        task3.setName("Task3");
        task4.setName("Task4");
        task5.setName("Task5");
        List<Task> tasks = Arrays.asList(task1, task2, task3);
        taskClient.save(tasks);
        final List<String> tasksAll = taskClient.get().stream().map(Task::getId).collect(Collectors.toList());
        final List<String> taskIds = Arrays.asList(task1.getId(), task2.getId(), task3.getId(), task4.getId(), task5.getId());
        resList = tasksAll.stream().distinct().filter(taskIds::contains).collect(Collectors.toList());
    }

    @After
    public void clean() {
        final List<String> tasksAll = taskClient.get().stream().map(Task::getId).collect(Collectors.toList());
        final List<String> taskIds = Arrays.asList(task1.getId(), task2.getId(), task3.getId(), task4.getId(), task5.getId());
        resList = tasksAll.stream().distinct().filter(taskIds::contains).collect(Collectors.toList());
        taskClient.delete(resList);
    }

    @Test
    public void post() {
        Assert.assertEquals(3, resList.size());

        taskClient.delete(Arrays.asList(task1.getId(), task2.getId()));

        final List<String> tasks = taskClient.get().stream().map(Task::getId).collect(Collectors.toList());
        final List<String> taskIds = Arrays.asList(task1.getId(), task2.getId(), task3.getId());
        final List<String> resList2 = tasks.stream().distinct().filter(taskIds::contains).collect(Collectors.toList());
        Assert.assertEquals(1, resList2.size());
    }

    @Test
    public void delete() {
        Assert.assertEquals(3, resList.size());

        taskClient.delete(Arrays.asList(task1.getId(), task2.getId()));

        List<String> tasks = taskClient.get().stream().map(Task::getId).collect(Collectors.toList());
        List<String> taskIds = Arrays.asList(task1.getId(), task2.getId(), task3.getId());
        List<String> resList2 = tasks.stream().distinct().filter(taskIds::contains).collect(Collectors.toList());
        Assert.assertEquals(1, resList2.size());
    }

    @Test
    public void get() {
        Assert.assertEquals(3, resList.size());
    }

}
