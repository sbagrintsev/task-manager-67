package ru.tsc.sbagrintsev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.stereotype.Controller;
import ru.tsc.bagrintsev.tm.api.client.ProjectCollectionRestEndpointClient;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.sbagrintsev.tm.marker.IntegrationCategory;

import java.util.*;
import java.util.stream.Collectors;

@Controller
@Category(IntegrationCategory.class)
public class ProjectCollectionEndpointTest {

    @NotNull
    final ProjectCollectionRestEndpointClient projectClient = ProjectCollectionRestEndpointClient.client();

    @NotNull
    final Project project1 = new Project();

    @NotNull
    final Project project2 = new Project();

    @NotNull
    final Project project3 = new Project();

    @NotNull
    final Project project4 = new Project();

    @NotNull
    final Project project5 = new Project();

    List<String> resList = new ArrayList<>();

    @Before
    public void init() {
        project1.setName("Project1");
        project2.setName("Project2");
        project3.setName("Project3");
        project4.setName("Project4");
        project5.setName("Project5");
        List<Project> projects = Arrays.asList(project1, project2, project3);
        projectClient.save(projects);
        final List<String> projectsAll = projectClient.get().stream().map(Project::getId).collect(Collectors.toList());
        final List<String> projectIds = Arrays.asList(project1.getId(), project2.getId(), project3.getId(), project4.getId(), project5.getId());
        resList = projectsAll.stream().distinct().filter(projectIds::contains).collect(Collectors.toList());
    }

    @After
    public void clean() {
        final List<String> projectsAll = projectClient.get().stream().map(Project::getId).collect(Collectors.toList());
        final List<String> projectIds = Arrays.asList(project1.getId(), project2.getId(), project3.getId(), project4.getId(), project5.getId());
        resList = projectsAll.stream().distinct().filter(projectIds::contains).collect(Collectors.toList());
        projectClient.delete(resList);
    }

    @Test
    public void post() {
        Assert.assertEquals(3, resList.size());

        projectClient.delete(Arrays.asList(project1.getId(), project2.getId()));

        final List<String> projects = projectClient.get().stream().map(Project::getId).collect(Collectors.toList());
        final List<String> projectIds = Arrays.asList(project1.getId(), project2.getId(), project3.getId());
        final List<String> resList2 = projects.stream().distinct().filter(projectIds::contains).collect(Collectors.toList());
        Assert.assertEquals(1, resList2.size());
    }

    @Test
    public void delete() {
        Assert.assertEquals(3, resList.size());

        projectClient.delete(Arrays.asList(project1.getId(), project2.getId()));

        List<String> projects = projectClient.get().stream().map(Project::getId).collect(Collectors.toList());
        List<String> projectIds = Arrays.asList(project1.getId(), project2.getId(), project3.getId());
        List<String> resList2 = projects.stream().distinct().filter(projectIds::contains).collect(Collectors.toList());
        Assert.assertEquals(1, resList2.size());
    }

    @Test
    public void get() {
        Assert.assertEquals(3, resList.size());
    }

}
