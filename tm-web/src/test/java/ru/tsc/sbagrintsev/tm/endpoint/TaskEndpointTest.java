package ru.tsc.sbagrintsev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.stereotype.Controller;
import ru.tsc.bagrintsev.tm.api.client.TaskRestEndpointClient;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.sbagrintsev.tm.marker.IntegrationCategory;

@Controller
@Category(IntegrationCategory.class)
public class TaskEndpointTest {

    @NotNull
    final TaskRestEndpointClient taskClient = TaskRestEndpointClient.client();

    @NotNull
    final Task task1 = new Task();

    @NotNull
    final Task task2 = new Task();

    @NotNull
    final Task task3 = new Task();

    @Before
    public void init() {
        task1.setName("Task1");
        taskClient.save(task1);
        task2.setName("Task2");
        taskClient.save(task2);
        task3.setName("Task3");
    }

    @After
    public void clean() {
        if (taskClient.get(task1.getId()) != null) taskClient.delete(task1.getId());
        if (taskClient.get(task2.getId()) != null) taskClient.delete(task2.getId());
        if (taskClient.get(task3.getId()) != null) taskClient.delete(task3.getId());
    }

    @Test
    public void post() {
        String id = task3.getId();
        Assert.assertNull(taskClient.get(id));
        taskClient.save(task3);
        Assert.assertEquals(task3.getName(), taskClient.get(id).getName());
    }

    @Test
    public void delete() {
        Assert.assertNotNull(taskClient.get(task1.getId()));
        taskClient.delete(task1.getId());
        Assert.assertNull(taskClient.get(task1.getId()));
    }

    @Test
    public void get() {
        Assert.assertEquals(task1.getName(), taskClient.get(task1.getId()).getName());
    }

}
