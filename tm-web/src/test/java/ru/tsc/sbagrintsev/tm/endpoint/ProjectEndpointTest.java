package ru.tsc.sbagrintsev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.stereotype.Controller;
import ru.tsc.bagrintsev.tm.api.client.ProjectRestEndpointClient;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.sbagrintsev.tm.marker.IntegrationCategory;

@Controller
@Category(IntegrationCategory.class)
public class ProjectEndpointTest {

    @NotNull
    final ProjectRestEndpointClient projectClient = ProjectRestEndpointClient.client();

    @NotNull
    final Project project1 = new Project();

    @NotNull
    final Project project2 = new Project();

    @NotNull
    final Project project3 = new Project();

    @Before
    public void init() {
        project1.setName("Project1");
        projectClient.save(project1);
        project2.setName("Project2");
        projectClient.save(project2);
        project3.setName("Project3");
    }

    @After
    public void clean() {
        if (projectClient.get(project1.getId()) != null) projectClient.delete(project1.getId());
        if (projectClient.get(project2.getId()) != null) projectClient.delete(project2.getId());
        if (projectClient.get(project3.getId()) != null) projectClient.delete(project3.getId());
    }

    @Test
    public void post() {
        String id = project3.getId();
        Assert.assertNull(projectClient.get(id));
        projectClient.save(project3);
        Assert.assertEquals(project3.getName(), projectClient.get(id).getName());
    }

    @Test
    public void delete() {
        Assert.assertNotNull(projectClient.get(project1.getId()));
        projectClient.delete(project1.getId());
        Assert.assertNull(projectClient.get(project1.getId()));
    }

    @Test
    public void get() {
        Assert.assertEquals(project1.getName(), projectClient.get(project1.getId()).getName());
    }

}
