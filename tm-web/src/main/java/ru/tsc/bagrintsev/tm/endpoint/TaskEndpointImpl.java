package ru.tsc.bagrintsev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.bagrintsev.tm.api.endpoint.TaskEndpoint;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.tsc.bagrintsev.tm.api.endpoint.TaskEndpoint")
@RestController
@NoArgsConstructor
@RequestMapping("/api/task")
public class TaskEndpointImpl implements TaskEndpoint {

    private TaskService taskService;

    @Autowired
    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    @WebMethod
    @PostMapping
    public void save(@RequestBody @WebParam(name = "tasks") Task task) {
        taskService.save(task);
    }

    @WebMethod
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") @WebParam(name = "id") String id) {
        taskService.deleteById(id);
    }

    @WebMethod
    @GetMapping("/{id}")
    public Task get(@PathVariable("id") @WebParam(name = "id") String id) {
        return taskService.findById(id);
    }

}
