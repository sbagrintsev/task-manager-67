package ru.tsc.bagrintsev.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.api.repository.TaskRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;

    @Transactional
    public void deleteAll(@NotNull final List<String> taskIds) {
        taskRepository.deleteAllById(taskIds);
    }

    @Transactional
    public void deleteById(@NotNull final String id) {
        taskRepository.deleteById(id);
    }

    @NotNull
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Nullable
    public Task findById(@NotNull final String id) {
        return taskRepository.findById(id).orElse(null);
    }

    @Transactional
    public void save(@NotNull final Task task) {
        taskRepository.save(task);
    }

    @Transactional
    public void saveAll(@NotNull final List<Task> tasks) {
        taskRepository.saveAll(tasks);
    }

}
