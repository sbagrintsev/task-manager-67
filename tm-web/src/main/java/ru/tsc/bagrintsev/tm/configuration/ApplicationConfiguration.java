package ru.tsc.bagrintsev.tm.configuration;

import lombok.NoArgsConstructor;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.hibernate.cfg.AvailableSettings;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.WebApplicationInitializer;
import ru.tsc.bagrintsev.tm.api.endpoint.ProjectCollectionEndpoint;
import ru.tsc.bagrintsev.tm.api.endpoint.ProjectEndpoint;
import ru.tsc.bagrintsev.tm.api.endpoint.TaskCollectionEndpoint;
import ru.tsc.bagrintsev.tm.api.endpoint.TaskEndpoint;
import ru.tsc.bagrintsev.tm.endpoint.ProjectCollectionEndpointImpl;
import ru.tsc.bagrintsev.tm.endpoint.ProjectEndpointImpl;
import ru.tsc.bagrintsev.tm.endpoint.TaskCollectionEndpointImpl;
import ru.tsc.bagrintsev.tm.endpoint.TaskEndpointImpl;
import ru.tsc.bagrintsev.tm.service.PropertyService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.sql.DataSource;
import javax.xml.ws.Endpoint;
import java.util.Properties;

@Configuration
@NoArgsConstructor
@EnableTransactionManagement
@ComponentScan("ru.tsc.bagrintsev.tm")
@EnableJpaRepositories("ru.tsc.bagrintsev.tm.api.repository")
public class ApplicationConfiguration implements WebApplicationInitializer {

    @Nullable
    private PropertyService propertyService;

    @Autowired
    public void setPropertyService(@NotNull final PropertyService propertyService) {
        this.propertyService = propertyService;
    }

    private Properties additionalProperties() {
        Properties properties = new Properties();
        assert propertyService != null;
        properties.setProperty(AvailableSettings.DIALECT, propertyService.getDatabaseSqlDialect());
        properties.setProperty(AvailableSettings.HBM2DDL_AUTO, propertyService.getDatabaseHbm2DdlAuto());
        properties.setProperty(AvailableSettings.SHOW_SQL, propertyService.getDatabaseShowSql());
        return properties;
    }

    @Bean
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Bean
    public Endpoint projectEndpointImplRegistry(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint projectCollectionEndpointImplRegistry(
            @NotNull final ProjectCollectionEndpoint projectEndpoint,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, projectEndpoint);
        endpoint.publish("/ProjectCollectionEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointImplRegistry(
            @NotNull final TaskEndpoint taskEndpoint,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskCollectionEndpointImplRegistry(
            @NotNull final TaskCollectionEndpoint taskEndpoint,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, taskEndpoint);
        endpoint.publish("/TaskCollectionEndpoint");
        return endpoint;
    }

    @Bean
    @NotNull
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDatabaseDriver());
        dataSource.setUsername(propertyService.getDatabaseUserName());
        dataSource.setPassword(propertyService.getDatabaseUserPassword());
        dataSource.setUrl(propertyService.getDatabaseUrl());
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource());
        factoryBean.setPackagesToScan("ru.tsc.bagrintsev.tm.model");
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setJpaProperties(additionalProperties());
        return factoryBean;
    }

    @Bean
    @NotNull
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        @NotNull final CXFServlet cxfServlet = new CXFServlet();
        @NotNull final ServletRegistration.Dynamic dynamicCXF = servletContext.addServlet("cxfServlet", cxfServlet);
        dynamicCXF.addMapping("/ws/*");
        dynamicCXF.setLoadOnStartup(1);
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager() {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }

}
