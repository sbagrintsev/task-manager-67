package ru.tsc.bagrintsev.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.tsc.bagrintsev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface TaskEndpoint {

    @WebMethod
    @PostMapping(produces = "application/json")
    void save(@RequestBody @WebParam(name = "task") Task task);

    @WebMethod
    @DeleteMapping(value = "/{id}" , produces = "application/json")
    void delete(@PathVariable("id") @WebParam(name = "id") String id);

    @WebMethod
    @GetMapping(value = "/{id}" , produces = "application/json")
    Task get(@PathVariable("id") @WebParam(name = "id") String id);

}
