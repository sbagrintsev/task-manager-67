package ru.tsc.bagrintsev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.bagrintsev.tm.api.endpoint.ProjectEndpoint;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@NoArgsConstructor
@RequestMapping("/api/project")
@WebService(endpointInterface = "ru.tsc.bagrintsev.tm.api.endpoint.ProjectEndpoint")
public class ProjectEndpointImpl implements ProjectEndpoint {

    private ProjectService projectService;

    @Autowired
    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    @WebMethod
    @PostMapping
    public void save(@RequestBody @WebParam(name = "project") Project project) {
        projectService.save(project);
    }

    @WebMethod
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") @WebParam(name = "id") String id) {
        projectService.deleteById(id);
    }

    @WebMethod
    @GetMapping("/{id}")
    public Project get(@PathVariable("id") @WebParam(name = "id") String id) {
        return projectService.findById(id);
    }

}
