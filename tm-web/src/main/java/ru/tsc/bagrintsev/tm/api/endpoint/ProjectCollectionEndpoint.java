package ru.tsc.bagrintsev.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.tsc.bagrintsev.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ProjectCollectionEndpoint {

    @WebMethod
    @PostMapping(produces = "application/json")
    void save(@RequestBody @WebParam(name = "projects") List<Project> projects);

    @WebMethod
    @DeleteMapping(produces = "application/json")
    void delete(@RequestBody @WebParam(name = "projects") List<String> projectIds);

    @WebMethod
    @GetMapping(produces = "application/json")
    List<Project> get();

}
