package ru.tsc.bagrintsev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.bagrintsev.tm.api.endpoint.TaskCollectionEndpoint;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@NoArgsConstructor
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.tsc.bagrintsev.tm.api.endpoint.TaskCollectionEndpoint")
public class TaskCollectionEndpointImpl implements TaskCollectionEndpoint {

    private TaskService taskService;

    @Autowired
    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    @WebMethod
    @PostMapping
    public void save(@RequestBody @WebParam(name = "tasks") List<Task> tasks) {
        taskService.saveAll(tasks);
    }

    @WebMethod
    @DeleteMapping
    public void delete(@RequestBody @WebParam(name = "tasks") List<String> taskIds) {
        taskService.deleteAll(taskIds);
    }

    @WebMethod
    @GetMapping
    public List<Task> get() {
        return taskService.findAll();
    }

}
