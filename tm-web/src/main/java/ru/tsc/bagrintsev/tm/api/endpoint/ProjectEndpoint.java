package ru.tsc.bagrintsev.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.tsc.bagrintsev.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ProjectEndpoint {

    @WebMethod
    @PostMapping(produces = "application/json")
    void save(@RequestBody @WebParam(name = "project") Project project);

    @WebMethod
    @DeleteMapping(value = "/{id}" , produces = "application/json")
    void delete(@PathVariable("id") @WebParam(name = "id") String id);

    @WebMethod
    @GetMapping(value = "/{id}" , produces = "application/json")
    Project get(@PathVariable("id") @WebParam(name = "id") String id);

}
