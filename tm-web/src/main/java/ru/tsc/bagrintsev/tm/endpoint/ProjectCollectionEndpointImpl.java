package ru.tsc.bagrintsev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.bagrintsev.tm.api.endpoint.ProjectCollectionEndpoint;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@NoArgsConstructor
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.tsc.bagrintsev.tm.api.endpoint.ProjectCollectionEndpoint")
public class ProjectCollectionEndpointImpl implements ProjectCollectionEndpoint {

    private ProjectService projectService;

    @Autowired
    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    @WebMethod
    @PostMapping
    public void save(@RequestBody @WebParam(name = "projects") List<Project> projects) {
        projectService.saveAll(projects);
    }

    @WebMethod
    @DeleteMapping
    public void delete(@RequestBody @WebParam(name = "projects") List<String> projectIds) {
        projectService.deleteAll(projectIds);
    }

    @WebMethod
    @GetMapping
    public List<Project> get() {
        return projectService.findAll();
    }

}
