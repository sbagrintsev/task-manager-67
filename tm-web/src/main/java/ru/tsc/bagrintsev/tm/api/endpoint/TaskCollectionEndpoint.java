package ru.tsc.bagrintsev.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.tsc.bagrintsev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface TaskCollectionEndpoint {

    @WebMethod
    @PostMapping(produces = "application/json")
    void save(@RequestBody @WebParam(name = "tasks") List<Task> tasks);

    @WebMethod
    @DeleteMapping(produces = "application/json")
    void delete(@RequestBody @WebParam(name = "tasks") List<String> taskIds);

    @WebMethod
    @GetMapping(produces = "application/json")
    List<Task> get();

}
