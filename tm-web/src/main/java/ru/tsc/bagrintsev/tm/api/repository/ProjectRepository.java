package ru.tsc.bagrintsev.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.bagrintsev.tm.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

}
