<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h1 style="margin-top: 20px;">TASK LIST</h1>

<table width="100%" cellpadding="10" border="1" style="border-collapse: collapse;">
    <tr>
        <th width="15%" nowrap="nowrap">ID</th>
        <th width="15%" nowrap="nowrap" align="left">NAME</th>
        <th width="30%" align="left">DESCRIPTION</th>
        <th width="10%" nowrap="nowrap" align="center">PROJECT</th>
        <th width="10%" nowrap="nowrap" align="center">STATUS</th>
        <th width="10%" nowrap="nowrap" align="center">START</th>
        <th width="10%" nowrap="nowrap" align="center">FINISH</th>
        <th width="10%" nowrap="nowrap" align="center">DELETE</th>
    </tr>

    <c:forEach var="task" items="${tasks}">
        <tr>
            <td>
                <c:out value="${task.id}"/>
            </td>
            <td>
                <a href="/task/edit/${task.id}">
                    <c:out value="${task.name}"/>
                </a>
            </td>
            <td>
                <c:out value="${task.description}"/>
            </td>
            <td align="center" nowrap="nowrap">
                    <%--@elvariable id="projects" type="java.util.Map"--%>
                <a href="/project/edit/${task.project.id}">
                    <c:out value="${projects[task.project.id].name}"/>
                </a>
            </td>
            <td align="center" nowrap="nowrap">
                <c:out value="${task.status.displayName}"/>
            </td>
            <td align="center">
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.dateStarted}"/>
            </td>
            <td align="center">
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.dateFinished}"/>
            </td>
            <td align="center">
                <a href="/task/delete/${task.id}">DELETE</a>
            </td>
        </tr>
    </c:forEach>

</table>

<form action="/task/create" style="margin-top: 20px; margin-left: 50px;">
    <button>CREATE TASK</button>
</form>

<jsp:include page="../include/_footer.jsp"/>