<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>

<h1 style="margin-top: 20px;">TASK EDIT</h1>

<%--@elvariable id="form" type="ru.tsc.bagrintsev.tm.dto.FormDTO"--%>
<form:form action="/task/edit/${form.task.id}" method="POST" modelAttribute="form">

    <form:hidden path="task.id"/>

    <form:hidden path="referer"/>

    <p>
        <form:label path="task.name">Name:</form:label>
        <form:input path="task.name"/>
    </p>

    <p>
        <form:label path="task.description">Description:</form:label>
        <form:input path="task.description"/>
    </p>

    <p>
        <form:label path="task.status">Status:</form:label>
        <form:select path="task.status">
            <form:option value="${null}" label="---//---"/>
            <form:options items="${form.statuses}" itemLabel="displayName"/>
        </form:select>
    </p>

    <p>
        <form:label path="task.dateStarted">Started:</form:label>
        <form:input type="date" path="task.dateStarted"/>
    </p>

    <p>
        <form:label path="task.dateFinished">Finished:</form:label>
        <form:input type="date" path="task.dateFinished"/>
    </p>

    <p>
        <form:label path="text">Project:</form:label>
        <form:select path="text">
            <form:option value="${null}" label="---//---"/>
            <%--@elvariable id="projects" type="java.util.List"--%>
            <form:options items="${projects}" itemLabel="name" itemValue="id"/>
        </form:select>
    </p>

    <form:button name="SAVE">SAVE</form:button>

</form:form>

<jsp:include page="../include/_footer.jsp"/>