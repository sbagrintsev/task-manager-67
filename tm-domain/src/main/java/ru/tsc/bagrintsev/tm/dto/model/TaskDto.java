package ru.tsc.bagrintsev.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "m_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class TaskDto extends AbstractWBSDtoModel {

    @Nullable
    @Column(name = "project_id")
    private String projectId;

}
