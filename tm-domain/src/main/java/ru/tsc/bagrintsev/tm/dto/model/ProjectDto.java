package ru.tsc.bagrintsev.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "m_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectDto extends AbstractWBSDtoModel {

}
