package ru.tsc.bagrintsev.tm.exception.entity;

public final class DomainNotFoundException extends AbstractEntityException {

    public DomainNotFoundException() {
        super("Error! Domain is empty...");
    }

}
