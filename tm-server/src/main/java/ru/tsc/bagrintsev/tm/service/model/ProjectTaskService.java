package ru.tsc.bagrintsev.tm.service.model;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.bagrintsev.tm.api.sevice.model.IProjectService;
import ru.tsc.bagrintsev.tm.api.sevice.model.IProjectTaskService;
import ru.tsc.bagrintsev.tm.api.sevice.model.ITaskService;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.repository.model.ProjectRepository;
import ru.tsc.bagrintsev.tm.repository.model.TaskRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final ProjectRepository projectRepository;

    @NotNull
    private final TaskRepository taskRepository;

    @Override
    @Transactional
    public Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws IdIsEmptyException, ProjectNotFoundException, TaskNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (projectId == null || projectId.isEmpty())
            throw new IdIsEmptyException(EntityField.PROJECT_ID.getDisplayName());
        if (taskId == null || taskId.isEmpty()) throw new IdIsEmptyException(EntityField.TASK_ID.getDisplayName());
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        if (!taskService.existsById(userId, taskId)) throw new TaskNotFoundException();
        return taskService.setProjectId(userId, taskId, projectId);
    }

    @Override
    @Transactional
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws IdIsEmptyException, ProjectNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (projectId == null || projectId.isEmpty())
            throw new IdIsEmptyException(EntityField.PROJECT_ID.getDisplayName());
        @Nullable final List<Task> tasks = taskService.findAllByProjectId(userId, projectId);
        if (tasks != null) tasks.forEach((t) -> taskRepository.deleteByUserIdAndId(userId, t.getId()));
        @Nullable Project project = projectRepository.findByUserIdAndId(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.deleteByUserIdAndId(userId, projectId);
    }

    @Override
    @Transactional
    public Task unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId
    ) throws IdIsEmptyException, TaskNotFoundException, ProjectNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (taskId == null || taskId.isEmpty()) throw new IdIsEmptyException(EntityField.TASK_ID.getDisplayName());
        return taskService.setProjectId(userId, taskId, null);
    }

}
