package ru.tsc.bagrintsev.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.tsc.bagrintsev.tm.api.sevice.ISenderService;
import ru.tsc.bagrintsev.tm.dto.EventMessage;

import javax.jms.*;
import javax.persistence.Table;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class SenderService implements ISenderService {

    @NotNull
    private static final String QUEUE = "TM_QUEUE";

    private static final int THREAD_COUNT = 3;

    @NotNull
    private final ExecutorService executorService = Executors.newFixedThreadPool(THREAD_COUNT);

    @NotNull
    private final Connection connection;

    @NotNull
    private final Session session;

    @NotNull
    private final MessageProducer producer;

    @NotNull
    private final BrokerService broker = new BrokerService();

    @SneakyThrows
    public SenderService() {
        initJMS();
        @NotNull final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createQueue(QUEUE);
        producer = session.createProducer(destination);
    }

    @Override
    public void createMessage(
            @NotNull final Object object,
            @NotNull final String eventType
    ) {
        executorService.submit(() -> sync(object, eventType));
    }

    public void initJMS() throws Exception {
        broker.addConnector(ActiveMQConnectionFactory.DEFAULT_BROKER_BIND_URL);
        broker.start();
    }

    private void send(@NotNull final String json) throws JMSException {
        final TextMessage message = session.createTextMessage(json);
        producer.send(message);
    }

    public void stop() throws JMSException {
        producer.close();
        session.close();
        connection.close();
        executorService.shutdown();
    }

    public void stopJMS() throws Exception {
        broker.stop();
    }

    @SneakyThrows
    private void sync(
            @NotNull final Object object,
            @NotNull final String eventType
    ) {
        @NotNull final EventMessage eventMessage = new EventMessage(object, eventType);
        if (object.getClass().isAnnotationPresent(Table.class)) {
            @NotNull final String tableName = object.getClass().getAnnotation(Table.class).name();
            eventMessage.setTableName(tableName);
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String messageJson;
        messageJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(eventMessage);
        send(messageJson);
    }

}
