package ru.tsc.bagrintsev.tm.api.sevice.dto;

import ru.tsc.bagrintsev.tm.dto.model.ProjectDto;

public interface IProjectDtoService extends IUserOwnedDtoService<ProjectDto> {

}
