package ru.tsc.bagrintsev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.tsc.bagrintsev.tm.api.sevice.model.IAbstractService;
import ru.tsc.bagrintsev.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractService<M extends AbstractModel> implements IAbstractService<M> {

    @NotNull
    public abstract List<M> findAll();

    @NotNull
    public abstract Collection<M> set(@NotNull final Collection<M> records);

    public abstract long totalCount();

}
