package ru.tsc.bagrintsev.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.exception.system.WrongDateFormatException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {

    @NotNull
    SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    @NotNull
    static Date toDate(final String value) throws WrongDateFormatException {
        try {
            return format.parse(value);
        } catch (ParseException e) {
            throw new WrongDateFormatException(e);
        }
    }

    @NotNull
    static String toString(final Date date) {
        if (date == null) return "";
        return format.format(date);
    }

}
