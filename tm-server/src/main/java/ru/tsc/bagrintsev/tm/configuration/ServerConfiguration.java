package ru.tsc.bagrintsev.tm.configuration;

import lombok.RequiredArgsConstructor;
import org.hibernate.cfg.AvailableSettings;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@RequiredArgsConstructor
@EnableTransactionManagement
@ComponentScan("ru.tsc.bagrintsev.tm")
@EnableJpaRepositories("ru.tsc.bagrintsev.tm.repository")
public class ServerConfiguration {

    @NotNull
    private final IPropertyService propertyService;

    private Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty(AvailableSettings.DIALECT, propertyService.getDatabaseSqlDialect());
        properties.setProperty(AvailableSettings.HBM2DDL_AUTO, propertyService.getDatabaseHbm2DdlAuto());
        properties.setProperty(AvailableSettings.C3P0_MAX_SIZE, propertyService.getDatabasePoolMaxSize());
        properties.setProperty(AvailableSettings.C3P0_MIN_SIZE, propertyService.getDatabasePoolMinSize());
        properties.setProperty(AvailableSettings.C3P0_TIMEOUT, propertyService.getDatabasePoolTimeout());
        properties.setProperty(AvailableSettings.SHOW_SQL, propertyService.getDatabaseShowSql());
        properties.setProperty(AvailableSettings.FORMAT_SQL, propertyService.getDatabaseFormatSql());
        properties.setProperty(AvailableSettings.USE_SQL_COMMENTS, propertyService.getDatabaseUseSqlComments());
        properties.setProperty(AvailableSettings.USE_SECOND_LEVEL_CACHE, propertyService.getDatabaseUseSecondLevelCache());
        properties.setProperty(AvailableSettings.CACHE_REGION_FACTORY, propertyService.getDatabaseCacheRegionFactoryClass());
        properties.setProperty(AvailableSettings.USE_QUERY_CACHE, propertyService.getDatabaseCacheUseQueryCache());
        properties.setProperty(AvailableSettings.USE_MINIMAL_PUTS, propertyService.getDatabaseCacheUseMinimalPuts());
        properties.setProperty(AvailableSettings.CACHE_REGION_PREFIX, propertyService.getDatabaseCacheRegionPrefix());
        return properties;
    }

    @Bean
    @NotNull
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDatabaseDriver());
        dataSource.setUsername(propertyService.getDatabaseUserName());
        dataSource.setPassword(propertyService.getDatabaseUserPassword());
        dataSource.setUrl(propertyService.getDatabaseUrl());
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource());
        factoryBean.setPackagesToScan("ru.tsc.bagrintsev.tm.dto.model", "ru.tsc.bagrintsev.tm.model");
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setJpaProperties(additionalProperties());
        return factoryBean;
    }

    @Bean
    @NotNull
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager() {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }

}
