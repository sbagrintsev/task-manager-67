package ru.tsc.sbagrintsev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDto;
import ru.tsc.bagrintsev.tm.dto.model.TaskDto;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

@Category(DBCategory.class)
public final class ProjectTaskDtoServiceTest extends AbstractDtoTest {

    @NotNull
    private final String userId = "testUserId1";

    @Before
    public void setUp() throws ModelNotFoundException, IdIsEmptyException {
        @NotNull final ProjectDto project = new ProjectDto();
        project.setId("projectId1");
        project.setName("projectName1");
        projectService.add(userId, project);
        @NotNull final TaskDto task = new TaskDto();
        task.setId("taskId1");
        task.setName("taskName1");
        taskService.add(userId, task);
    }

    @Test
    @Category(DBCategory.class)
    public void testBindTaskToProject() throws AbstractException {
        Assert.assertNull(taskService.findAll().get(0).getProjectId());
        projectTaskServiceDTO.bindTaskToProject(userId, "projectId1", "taskId1");
        Assert.assertEquals("projectId1", taskService.findOneById(userId, "taskId1").getProjectId());
    }

    @Test
    @Category(DBCategory.class)
    public void testRemoveProjectById() throws AbstractException {
        Assert.assertFalse(projectService.findAll().isEmpty());
        Assert.assertFalse(taskService.findAll().isEmpty());
        projectTaskServiceDTO.bindTaskToProject(userId, "projectId1", "taskId1");
        projectTaskServiceDTO.removeProjectById(userId, "projectId1");
        Assert.assertTrue(projectService.findAll().isEmpty());
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void testUnbindTaskFromProject() throws AbstractException {
        projectTaskServiceDTO.bindTaskToProject(userId, "projectId1", "taskId1");
        Assert.assertEquals("projectId1", taskService.findOneById(userId, "taskId1").getProjectId());
        projectTaskServiceDTO.unbindTaskFromProject(userId, "taskId1");
        Assert.assertNull(taskService.findAll().get(0).getProjectId());
    }

}
