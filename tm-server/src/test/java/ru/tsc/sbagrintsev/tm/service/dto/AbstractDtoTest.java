package ru.tsc.sbagrintsev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IProjectDtoService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IProjectTaskDtoService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.ITaskDtoService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IUserDtoService;
import ru.tsc.bagrintsev.tm.configuration.ServerConfiguration;
import ru.tsc.bagrintsev.tm.dto.model.UserDto;
import ru.tsc.bagrintsev.tm.exception.user.LoginAlreadyExistsException;
import ru.tsc.bagrintsev.tm.exception.user.LoginIsIncorrectException;
import ru.tsc.bagrintsev.tm.exception.user.PasswordIsIncorrectException;
import ru.tsc.bagrintsev.tm.repository.dto.ProjectDtoRepository;
import ru.tsc.bagrintsev.tm.repository.dto.TaskDtoRepository;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;

@Controller
@Category(DBCategory.class)
public abstract class AbstractDtoTest {

    @NotNull
    public static final AnnotationConfigApplicationContext CONTEXT = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    protected IPropertyService propertyService = CONTEXT.getBean(IPropertyService.class);

    @NotNull
    protected ITaskDtoService taskService = CONTEXT.getBean(ITaskDtoService.class);

    @NotNull
    protected IProjectDtoService projectService = CONTEXT.getBean(IProjectDtoService.class);

    @NotNull
    protected IUserDtoService userService = CONTEXT.getBean(IUserDtoService.class);

    @NotNull
    protected ProjectDtoRepository projectRepository = CONTEXT.getBean(ProjectDtoRepository.class);

    @NotNull
    protected TaskDtoRepository taskRepository = CONTEXT.getBean(TaskDtoRepository.class);

    @NotNull
    protected IProjectTaskDtoService projectTaskServiceDTO = CONTEXT.getBean(IProjectTaskDtoService.class);

    @After
    public void destroy() {
        taskService.clearAll();
        projectService.clearAll();
        userService.clearAll();
    }

    @Before
    public void init() throws LoginIsIncorrectException, GeneralSecurityException, LoginAlreadyExistsException, PasswordIsIncorrectException {
        @NotNull final UserDto user1 = userService.create("test1", "testPassword1");
        user1.setId("testUserId1");
        @NotNull final UserDto user2 = userService.create("test2", "testPassword2");
        user2.setId("testUserId2");
        @NotNull final List<UserDto> list = Arrays.asList(user1, user2);
        userService.clearAll();
        userService.set(list);
    }

}
